#include <PEP30060101.h> //! scheda MCU

//! **********************************************************
// canali ADC di lettura del dipswitc
#define adc_low_nibble_addr 7
#define adc_high_nibble_addr 6


//! codifica indirizzi:
//! per semplicit� codifico solo 5 indirizzi dati dagli switch <5-8>
//! 0000 -> 0V
//! 0001 -> 1.78V
//! 0010 -> 2V
//! 0100 -> 2.27V
//! 1000 -> 2.5V
//! V=5V | 12bit | 5/4096=0.00122V/bit
//! imposto le soglie a met� tra 2 livelli
//! 0.9 | 1.89 | 2.135 | 2.38
//! 737 | 1548 | 1749  | 1950
#INLINE
int16 read_adc_addr(int8 channel)
{
 int8 i=0;
 int16 addr=0;
 set_adc_channel(channel);
 for(i=0;i<16;i++)
 {
  delay_us(10);
  addr=addr+read_adc();
 }
 addr=addr>>4; //divido per 16
 return(addr);
}

//! **********************************************************
//! versione ridotta
/*
#INLINE
int8 def_adc_addr(unsigned int16 adc_aux)
{
   if(adc_aux<737) 
   {return(0x00);}
   else if(adc_aux<1548)
   {return(0x01);}
   else if(adc_aux<1749)
   {return(0x02);}
   else if(adc_aux<1950)
   {return(0x04);}
   else
   {return(0x08);}
}
*/
//! **********************************************************
//! versione completa
#INLINE
int8 def_adc_addr(unsigned int16 adc_aux)
{
   if(adc_aux<731) 
   {return(0x00);}
   else if(adc_aux<1550)
   {return(0x01);}
   else if(adc_aux<1749)
   {return(0x02);}
   else if(adc_aux<1950)
   {return(0x04);}
   else if(adc_aux<2150)
   {return(0x08);}
   else if(adc_aux<2316)
   {return(0x03);}
   else if(adc_aux<2419)
   {return(0x05);}
   else if(adc_aux<2475)
   {return(0x06);}
   else if(adc_aux<2526)
   {return(0x09);}
   else if(adc_aux<2604)
   {return(0x0A);}
   else if(adc_aux<2702)
   {return(0x0C);}
   else if(adc_aux<2789)
   {return(0x07);}
   else if(adc_aux<2855)
   {return(0x0B);}
   else if(adc_aux<2906)
   {return(0x0D);}
   else if(adc_aux<3005)
   {return(0x0E);}
   else
   {return(0x0F);}
}

//! **********************************************************

#inline
int8 get_hw_address(void)
{
   int16 aux;
   int8 dev_addr;
   aux=read_adc_addr(adc_low_nibble_addr);
   dev_addr=def_adc_addr(aux);
   aux=read_adc_addr(adc_high_nibble_addr);
   dev_addr=dev_addr+(def_adc_addr(aux)<<4);
   return(dev_addr);
}

//! **********************************************************



//! **********************************************************

// DEBUG

void stato_led(int8 led)
{
   if(led>7)
      led=0;
   
   switch(led)
   {
      case 0:
         output_low(ST1);
         output_low(ST2);
         output_low(ST3);
      break;
      
      case 1:
         output_high(ST1);
         output_low(ST2);
         output_low(ST3);      
      break;
      
      case 2:
         output_low(ST1);
         output_high(ST2);
         output_low(ST3);      
      break;
      
      case 3:
         output_high(ST1);
         output_high(ST2);
         output_low(ST3);      
      break;
      
      case 4:
         output_low(ST1);
         output_low(ST2);
         output_high(ST3);
      break;
      
      case 5:
         output_high(ST1);
         output_low(ST2);
         output_high(ST3);      
      break;
      
      case 6:
         output_low(ST1);
         output_high(ST2);
         output_high(ST3);      
      break;
      
      case 7:
         output_high(ST1);
         output_high(ST2);
         output_high(ST3);      
      break;   
      
      default:
      break;
   }
}

