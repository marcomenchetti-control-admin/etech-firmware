//! In questa libreria ci sono le funzioni di gestione delle stringhe GPS RMC
#include<math.h>
#define MAX_CHAR_GPS 100

unsigned int8 n_RMC=0; // numero carattere ricevuto
char gps_in_buffer[MAX_CHAR_GPS];

#define Max_Pacchetti 15
#define Max_Lenght 15
int1 stato_ric=0; // 0 in attesaa, 1 durante ricezione
char pacchetto_in[Max_Pacchetti][Max_Lenght];
int8 num_pacchetti=0;
int1 pacchetto_RMC=0;
int1 checksum_valid=false;

unsigned int32 data=0;
unsigned int8 giorno=0;
unsigned int8 mese=0;
unsigned int8 anno=0;

unsigned int8 lat_dir=0;
unsigned int8 lat_deg=0;
unsigned int8 lat_min=0;
unsigned int16 lat_sec=0;

unsigned int8 lon_dir=0;
unsigned int8 lon_deg=0;
unsigned int8 lon_min=0;
unsigned int16 lon_sec=0;

unsigned int8 ora=0;
unsigned int8 minuti=0;
unsigned int8 secondi=0;
unsigned int8 centesimi=0;

unsigned int8 fix=0;
unsigned int16 speed=0;
unsigned int16 compass=0;
/*
[0] $GPRMC
[1] hhmmss.sss
[2] A : data valid | V : data not valid
[3] ddmm.mmmm //! Lat
[4] N/S
[5] ddmm.mmmm //! Lon
[6] E/W
[7] Speed over grond [knot]
[8] Course over ground [degrees]
[9] ddmmyy //! date
[10] 3.05 [degree] //! Magnetic variation
[11] W  //! Magnetic variation
[12] A*XX //! Mode and checksum 
*/

int8 ricevutoGPS=FALSE; //FLAG MI IDENTIFICA QUANDO HO UNA STRINGA RMC COMPLETA 

void separazione_campi(void);
void purge_buffer_in(void);
void purge_pacchetti(void);
void init_GPS(void);
int8 elaborazione_pacchetti(void);
int8 get_checksum(char*,int8);
int8 compute_checksum(char*);
int8 to_hex(int8);


//!-----------------------------------------------------

//! SUDDIVISIONE DELLA STRINGA ACQUISITA IN PACCHETTI
void separazione_campi(void)
{   
   int8 i=0;
   char *ptr;
   char separatore[1];
   
   strcpy(separatore,",");
   
   ptr = strtok(gps_in_buffer, separatore);
   
   while(ptr!=0) 
   {
    sprintf(pacchetto_in[i++],ptr);
    ptr = strtok(0, separatore);
   }
   num_pacchetti=i;
}

//!-----------------------------------------------------

char float_part[2][6];

//! SUDDIVISIONE DELLA STRINGA FLOAT IN STRINGA INTERI e STRINGA DECIMALI
void separazione_campi_float(char *string_in)
{   
   int8 i=0;
   char *ptr;
   char separatore[1];
   
   strcpy(separatore,".");
   
   ptr = strtok(string_in, separatore);
   
   while(ptr!=0) 
   {
    sprintf(float_part[i++],ptr);
    ptr = strtok(0, separatore);
   }
   num_pacchetti=i;
}

//!-----------------------------------------------------
/*
RISPOSTA COMPOSTA DA 4 FRAME_ID
risp_1_id 0x00000304 payload 8
   [0] addr_hw
   [1] day (1..31)
   [2] month (1..12)
   [3] year (0..99)
   [4] hour (0..23)
   [5] minute (0..59)
   [6] second (0..59)
   [7] 1/100 second (0..99)
risp_2_id 0x00000305 payload 6
   [0] addr_hw
   [1] latitude dir (N/S)
   [2] latitude deg (0..90)
   [3] latitude deg_min (0..59)
   [4][5] latitude deg_sec (0..5999)
   [6][7] speed (0..65365) [mm/s] 235Km/h max
risp_3_id 0x00000306 payload 6
   [0] addr_hw
   [1] longitude dir (E/W)
   [2] longitude deg (0..180)
   [3] longitude deg_min (0..60)
   [4][5] longitude deg_sec (0..5999)
   [6][7] compass (0..3599) [1/10 deg]
risp_4_id 0x00000307 payload 6
   [0] addr_hw
   [1] FIX (V) 
*/
int8 elaborazione_pacchetti(void)
{
   unsigned int32 aux=0;
   float aux_f=0.0;
   char aux_s[10];
   disable_interrupts(int_RDA);
   //! fix [2]
   strcpy(aux_s,"$GPRMC\n");
   if(strcmp(pacchetto_in[0],aux_s)) //! pacchetto RMC
   { 
      fix=pacchetto_in[2][0];
      if(fix=='A' && checksum_valid) //! Raggiunto il FIX
      {
          output_high(ST3);
          
          //! ora [1]
          aux_f = atof(pacchetto_in[1]); // get the float value from the string="hhmmss.cc"
          aux = floor(aux_f); // get the integer value from the string: "hhmmss"
          ora = aux/10000; // get "hh" from aux
          minuti = (aux/100 - ora*100); // get "mm" from aux
          secondi = (aux - ora*10000 - minuti*100); // get "ss" from aux
          centesimi = ((aux_f-aux)*100.0); //get "cc" from string
          
          //! lat [3] //TODO:check
          aux_f = atof(pacchetto_in[3]); // get the float value for LAT in DDmm.mmmmm
          lat_deg = floor( aux_f/100 );
          aux_f -= (float)lat_deg*100; // contains mm.mmmmm
          lat_min = floor( aux_f );
          lat_sec = ( aux_f - lat_min )*60000; // save sec*100
          
          //! Dir Lat [4]
          lat_dir=pacchetto_in[4][0];
          
          //! lon [5]
          aux_f = atof(pacchetto_in[5]); // get the float value for LAT in DDmm.mmmmm
          lon_deg = floor( aux_f/100 );
          aux_f -= (float)lon_deg*100; // contains mm.mmmmm
          lon_min = floor( aux_f );
          lon_sec = ( aux_f - lon_min )*60000; // save sec*100
          
          //! Dir lon [6]
          lon_dir=pacchetto_in[6][0];
          
          //! vel [7] //! espressa in knot 0,514m/s
          aux_f=atof(pacchetto_in[7]); // espresso in knot
          //aux_f=aux_f*0.514; // [mt/sec];
          //speed=aux_f*1000;  // [mm/sec], fondoscala 235Km/h
          speed = aux_f*514.4; // (aux_f[knots] * 0.5144) = speed[m]*[s]^-1 -> speed[m]*[s]^-1 * 1000 = speed [mm]*[s]^-1 -> aux_f[knot] * 514.4 = speed [mm]*[s]^-1
           
          //! course over ground [8]
          aux_f=atof(pacchetto_in[8]); //! [deg]
          aux_f=aux_f*10; //! [1/10 deg]
          compass=aux_f;
          
          //! data [9]
          char zero = 0x30; // to move from char to int
          giorno = (pacchetto_in[9][0]-zero)*10; // day decine
          giorno += (pacchetto_in[9][1]-zero);   // day
          mese = (pacchetto_in[9][2]-zero)*10; // month decine
          mese += (pacchetto_in[9][3]-zero);   // month
          anno = (pacchetto_in[9][4]-zero)*10; // year decine
          anno += (pacchetto_in[9][5]-zero);   // year
      }
      else //! NO FIX esco dalla funzione
      {
        output_low(ST3);
        enable_interrupts(int_RDA);
        return TRUE;
      }
      
   }
   else // pacchetto arrivato non � RMC
   {
      enable_interrupts(int_RDA);
      return FALSE;
   }
   enable_interrupts(int_RDA);
   return TRUE;
}

//!-----------------------------------------------------

//! PULISCE LA STRINGA IN INGRESSO
void purge_buffer_in(void)
{
 int8 aux;
 for(aux=0;aux<MAX_CHAR_GPS;aux++)
 {gps_in_buffer[aux]='\0';}
}   

//!-----------------------------------------------------

//! PULISCE I PACCHETTI
void purge_pacchetti(void)
{
 int8 aux;
 int8 aux2;
 for(aux=0;aux<Max_Pacchetti;aux++)
 {
  for(aux2=0;aux2<Max_Lenght;aux2++)
  {
   pacchetto_in[aux][aux2]='\0';
  }
 }
}   

//!-----------------------------------------------------

void init_GPS(int8 up_rate)
{
//! set NMEA sentence (RMC only)   
   fprintf(PORT1,"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29");
//! Risposta attesa "$PMTK001,314,3*30"
   fputc(13,PORT1);
   fputc(10,PORT1);
   delay_ms(200); 
//! set update rate 
   if(up_rate==0x01)
   {
    fprintf(PORT1,"$PMTK220,1000*1F"); //1Hz
   }
   else if(up_rate==0x05)
   {
    fprintf(PORT1,"$PMTK220,200*2C");  //5Hz
   }
   else if(up_rate==0x0A)
   {
     fprintf(PORT1,"$PMTK220,100*2F");  //10Hz
   }
   else
   {
    fprintf(PORT1,"$PMTK220,1000*1F"); //1Hz
   }   
//! Risposta attesa "$PMTK001,220,3*30"
   fputc(13,PORT1);
   fputc(10,PORT1);
   delay_ms(200); 
//! disable velocity limit
   fprintf(PORT1,"$PMTK397,0*23");
   //! $PMTK397,0.2*3F 
   //! $PMTK397,2.0*3F
   fputc(13,PORT1);
   fputc(10,PORT1);
   delay_ms(200); 
   
   
}

//!-----------------------------------------------------

#int_RDA //GPS
void  RDA_isr(void) {
   char carattereGPS;
   carattereGPS=fgetc(PORT1);
   
//!   #ifdef debug
//!      fputc(carattereGPS,icd_PORT);
//!   #endif

   /**
    * Deciding if starting or finishing. If in between simply skip
    */
   if(carattereGPS == '$'/*start_char*/ && !stato_ric/*not receiving*/) {
      /**
       * should not be necessary since the starting condition should verify 
       * only after the stopping condition. However this might prevent errors:
       *
       * n_RMC = 0;
       *
       */
      output_high(ST2);
      stato_ric = 1;
   } else if(carattereGPS == 0x0A/*end_char*/ && stato_ric/*receiving*/) {
	  int8 cksm_got = get_checksum(gps_in_buffer,n_RMC);
	  int8 cksm_comp = compute_checksum(gps_in_buffer);
	  checksum_valid = cksm_got == cksm_comp;
      stato_ric = 0;
      n_RMC = 0;
      output_low(ST2);
      ricevutoGPS = true;
   } else {
      stato_ric = 1; // prevent the code from getting stuck.
   }
   /**
    * Positioning of input character.
    */
   if(n_RMC < MAX_CHAR_GPS/*buffer still empty*/ && stato_ric/*receiving*/) {
      gps_in_buffer[n_RMC++] = carattereGPS; // add the character to the buffer
   } else if( n_RMC >= MAX_CHAR_GPS && stato_ric ) {
      //lost the ending message.
      n_RMC = 0; //resetting buffer (not really but still...)
      stato_ric = 0; // stopping reception of messages
   } else { // basically here stato_ric is 0 meaning that
      //message was shorter than expected: is there something to do?
   }
}

//!-----------------------------------------------------
int8 get_checksum(char nmea_input_string[], int8 n_chars) {
	char cksm = to_hex( nmea_input_string[ n_chars-3 ] ) << 4;
	cksm |= to_hex( nmea_input_string[ n_chars-2 ] );
	return cksm;
}
//!-----------------------------------------------------
int8 compute_checksum(char nmea_input_string[]) {
	int8 index = 0;
	char cksm = 0;
	while(nmea_input_string[++index] != 0x2A/* hex for '*' */) {
		cksm ^= nmea_input_string[index];
	}
	return cksm;
}
//!-----------------------------------------------------
int8 to_hex(int8 in) {
    if(in < 0x41/*10*/) return in - 0x30/*char for '0'*/;
    else return (in - 0x41/*char for A*/) + 10;
}
