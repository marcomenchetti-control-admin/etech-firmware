unsigned int8 act_vel_A=0;
unsigned int8 act_dir_A=0;
unsigned int8 act_vel_B=0;
unsigned int8 act_dir_B=0;


void set_vel(unsigned int8 dirA,unsigned int8 velA,unsigned int8 dirB,unsigned int8 velB);
void init_acc(void);
void enable_vel(int1 en);
void float_to_IEEEf(int32 *in,int32 *out);
void IEEEf_to_float(int32 *in,int32 *out);

int1 stato_connessione=FALSE;

// zona morta 2400..2600

//! 1tick=1,39mV
//! 2500mV/1,39=1798,5->1800
//!int16 v_offset=2500; //! offset della velocit� impostabile dall'utente
int16 v_offset=2048;
//! **********************************************************
// VEL_A=192+11*DATA
// VEL_B=388+32*DATA

//! BIT DI CONFIGURAZIONE PER IL DAC
//! <A#/B><BUF><GA#><SHDN#><D11-D0>
//!unsigned int16 conf_A=6;
unsigned int16 conf_A=0b0011000000000000;
// VEL_A=192+11*DATA
//!unsigned int16 conf_B=14;
unsigned int16 conf_B=0b1011000000000000;
// VEL_B=388+32*DATA
//! END BIT DI CONFIGURAZIONE PER IL DAC

void set_vel(unsigned int8 dirA,unsigned int8 velA,unsigned int8 dirB,unsigned int8 velB)
{   
   signed int16 aux=0;
   unsigned int16 vel_a=0;
   unsigned int16 vel_b=0;
   //! per sicurezza in caso di dato errato metto al minimo
   //disable_interrupts(GLOBAL);

   if(velA>100)
   {velA=100;}      

   if(velB>100)
   {velB=100;}    

   aux=velA;
   if(dirA==1) // reverse
      aux=-aux;

   vel_a=(v_offset)+20*aux;
   
   aux=velB;
   if(dirB==1) // reverse
      aux=-aux;

   vel_b=(v_offset)+20*aux;
   
   // controllo di non uscire dai limiti
   if(vel_b>=4095)
   {vel_b=4095;}
   if(vel_a>=4095)
   {vel_a=4095;}
   
   aux=conf_A|vel_a;
   output_low(CS_ACC);
   spi_xfer(SPI_ACC,aux,16);
   output_high(CS_ACC);
   delay_us(10);

   aux=conf_B|vel_b;
   output_low(CS_ACC);
   spi_xfer(SPI_ACC,aux,16);
   output_high(CS_ACC);
   delay_us(10);
   
   //! IMPULSO SEGNALE LDAC
   output_low(LDAC);
   delay_us(10);
   output_high(LDAC);
   //enable_interrupts(GLOBAL);
}

//! **********************************************************

void init_acc(void)
{
   stato_connessione=FALSE;
   set_vel(0,0,0,0);
}

//! **********************************************************
#define relay J3_8

void enable_vel(int1 en)
{
      if(en)
      {output_high(relay);}
      else
      {output_low(relay);}
}

//! **********************************************************


/* DEFINIZIONE ID CAN RX*/
// comandi per la scheda che controlla acceleratore e odometria  set_cmd_ID 0x01
#define SET_ACC_ID         0x00000602 // DATA[0]=hw_address | DATA[1]= DIR_A | DATA[2]= VEL_A | DATA[3]= H_POS | DATA[2]= L_POS
#define GET_ACC_ID         0x00000604 // DATA[0]=hw_address | DATA[1]= H_POS | DATA[2]= L_POS 

#define USE_GPS_VEL        0x0000061A
#define ENABLE_VEL_ID      0x0000061C

#define SET_V_OFFSET       0x00000624 // imposta il valore di v_offset
#define GET_V_OFFSET       0x00000626 // richiede il valore di v_offset

#define SET_GAIN           0x00000630
#define GET_GAIN           0x00000632

#define SET_TRGT_VEL       0x00000634
#define GET_TRGT_VEL       0x00000636
#define GET_CURR_VEL       0x00000638

#define SET_TRGT_OME       0x0000063A
#define GET_TRGT_OME       0x0000063C
#define OME_COEFF_ID       0x0000063E

#define ABSENT_GPS_CMD     0x00000601

void gestione_comando(unsigned int32 frame_ID,unsigned int8 *data)
{
  switch(frame_ID)
   {  
      case USE_GPS_VEL :
         if(*(data) == hw_address) {
            int1 cmd = bit_test(*(data+1),0);
            if(cmd != USE_CONTROL) {
               set_vel(0,0,0,0);
               USE_CONTROL = cmd;
            }
            CAN_OUT_ID = frame_ID + 1;
            CAN_OUT_DATA_LEN = 2;
            CAN_OUT_DATA[0] = hw_address;
            CAN_OUT_DATA[1] = (int8)USE_CONTROL;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case ENABLE_VEL_ID :
         if(*(data+0)==hw_address)
         {
            set_vel(0,0,0,0);
            enable_vel(bit_test(*(data+1),0));
            CAN_OUT_DATA[0]=hw_address;
            if(bit_test(*(data+1),0))
               {CAN_OUT_DATA[1]=0x01;}
            else
               {CAN_OUT_DATA[1]=0x00;}
            CAN_OUT_DATA_LEN=2;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }  
      break;
      
      case SET_ACC_ID :
         if(*(data+0)==hw_address)
         {
            act_dir_A=*(data+1);
            act_vel_A=*(data+2);
            act_dir_B=*(data+3);
            act_vel_B=*(data+4);
            CAN_OUT_DATA[0]=hw_address;
            if(!USE_CONTROL) {
               set_vel(act_dir_A,act_vel_A,act_dir_B,act_vel_B);
               CAN_OUT_DATA[1]=act_dir_A;
               CAN_OUT_DATA[2]=act_vel_A;
               CAN_OUT_DATA[3]=act_dir_B;
               CAN_OUT_DATA[4]=act_vel_B;
            }
            else {
               set_vel(0,0,0,0);
               CAN_OUT_DATA[1]=0;
               CAN_OUT_DATA[2]=0;
               CAN_OUT_DATA[3]=0;
               CAN_OUT_DATA[4]=0;
            }
            CAN_OUT_DATA_LEN=5;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }  
      break;
      
      case GET_ACC_ID :
         if(*(data+0)==hw_address)
         {
            CAN_OUT_DATA[0]=hw_address;
            CAN_OUT_DATA[1]=act_dir_A;
            CAN_OUT_DATA[2]=act_vel_A;
            CAN_OUT_DATA[3]=act_dir_B;
            CAN_OUT_DATA[4]=act_vel_B;
            CAN_OUT_DATA_LEN=5;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;         
 
      case SET_V_OFFSET :
         if(*(data+0)==hw_address)
         {
            v_offset = *((int16*)(data + 1));
            //msb(v_offset)=*(data+1);
            //lsb(v_offset)=*(data+2);
            CAN_OUT_DATA[0]=hw_address;
            CAN_OUT_DATA_LEN=1;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR); 
         }     
      break;  
  
      case GET_V_OFFSET :
         if(*(data+0)==hw_address)
         {
            CAN_OUT_DATA[0]=hw_address;
            *((int16*)(CAN_OUT_DATA+1)) = v_offset;
            //CAN_OUT_DATA[1]=msb(v_offset);
            //CAN_OUT_DATA[2]=lsb(v_offset);
            CAN_OUT_DATA_LEN=3;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;  
      
      case SET_TRGT_VEL :
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0]=hw_address;
            float_to_IEEEf((int32*)&current_vel,(int32*)(CAN_OUT_DATA+1));
            IEEEf_to_float((int32*)(data+1),(int32*)&target_vel);
            CAN_OUT_DATA_LEN=5;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case GET_TRGT_VEL :
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0]=hw_address;
            float_to_IEEEf((int32*)&target_vel,(int32*)(CAN_OUT_DATA+1));
            CAN_OUT_DATA_LEN=5;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case GET_CURR_VEL :
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0]=hw_address;
            float_to_IEEEf((int32*)&current_vel,(int32*)(CAN_OUT_DATA+1));
            CAN_OUT_DATA_LEN=5;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case SET_GAIN :
         if( *(data) == hw_address ) {
            CAN_OUT_DATA[0] = hw_address;
            CAN_OUT_DATA[1] = *(data+1);
            float32 dato;
            float32 dato_old;
            IEEEf_to_float((int32*)(data+2),(int32*)&dato);
            switch( *(data+1) ) {
               case 0:
                  dato_old = PID_controller_VEL.P;
                  PID_controller_VEL.P = dato;
               break;
               case 1:
                  dato_old = PID_controller_VEL.I * freq_s;
                  PID_controller_VEL.I = dato * T_s ;
               break;
               case 2:
                  dato_old = PID_controller_VEL.D * T_s;
                  PID_controller_VEL.D = dato * freq_s;
               break;
            }
            float_to_IEEEf((int32*)&dato_old,(int32*)(CAN_OUT_DATA+2));
            CAN_OUT_DATA_LEN = 6;
            CAN_OUT_ID = frame_ID + 1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case GET_GAIN :
         if( *data == hw_address ) {
            CAN_OUT_DATA[0] = hw_address;
            CAN_OUT_DATA[1] = *(data+1);
            float32 dato;
            switch( *(data+1) ) {
               case 0:
                  dato = PID_controller_VEL.P;
               break;
               case 1:
                  dato = PID_controller_VEL.I * freq_s;
               break;
               case 2:
                  dato = PID_controller_VEL.D * T_s;
               break;
            }
            
            float_to_IEEEf((int32*)&dato,(int32*)(CAN_OUT_DATA+2));
            CAN_OUT_DATA_LEN = 6;
            CAN_OUT_ID = frame_ID + 1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case SET_TRGT_OME:
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0] = hw_address;
            float_to_IEEEf((int32*)(&target_omega),(int32*)(CAN_OUT_DATA+1));
            
            CAN_OUT_DATA_LEN = 5;
            CAN_OUT_ID = frame_ID + 1;
            
            IEEEf_to_float((int32*)(data+1),(int32*)(&target_omega));
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case GET_TRGT_OME:
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0] = hw_address;
            float_to_IEEEf((int32*)(&target_omega),(int32*)(CAN_OUT_DATA+1));
            
            CAN_OUT_DATA_LEN = 5;
            CAN_OUT_ID = frame_ID + 1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      case OME_COEFF_ID:
         if(*(data) == hw_address) {
            CAN_OUT_DATA[0] = hw_address;
            float_to_IEEEf((int32*)(&omega_coefficient),(int32*)(CAN_OUT_DATA+1));
            
            CAN_OUT_DATA_LEN = 5;
            CAN_OUT_ID = frame_ID + 1;
            
            IEEEf_to_float((int32*)(data+1),(int32*)(&omega_coefficient));
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;
      
      default :
         CAN_OUT_ID = 0x600;
         CAN_OUT_DATA_LEN = 3;
         CAN_OUT_DATA[0] = hw_address;
         CAN_OUT_DATA[1] = 0x04; 
         CAN_OUT_DATA[2] = 0x04; // Command not found �\_(�-�)_/�
         can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
      break;
   }
}

void float_to_IEEEf(int32 *in, int32* out) {
   int32 aux = *in;
   int1 sign = bit_test(aux,15);
   int8 exp = *((int8*)&aux);
   int32 mant = 0;
   *((int8*)&mant) = *((int8*)&aux + 3);
   *((int8*)&mant + 1) = *((int8*)&aux + 2);
   *((int8*)&mant + 2) = *((int8*)&aux + 1) & 0x7f;
   *out = ((int32)sign << 31) | ((int32)exp << 23) | mant;
}

void IEEEf_to_float( int32 *in, int32 *out ) {
   int32 aux = *in;
   int1 sign = bit_test(aux,31);
   int8 exp = ((aux<<1) >> 24);
   int32 mant = ((aux<<9) >> 9 );
   *((int8*)out) = exp;
   *((int8*)out + 1) = *((int8*)&mant + 2);
   *((int8*)out + 2) = *((int8*)&mant + 1);
   *((int8*)out + 3) = *((int8*)&mant);
   *out = *out | ((int32)sign << 15);
}
