#if SET_CMD_ID == Acceleratore
   #include <18F46K80.h>
#elif SET_CMD_ID == GPS
   #include <18F46K80.h>
#endif

#device ADC=12

#FUSES WDT                      //Watch Dog Timer
#FUSES WDT256                   //Watch Dog Timer uses 1:256 Postscale
#FUSES NOBROWNOUT               //No brownout reset
#FUSES BORV30                   //Brownout reset at 3.0V

#use delay(clock=64MHz,crystal=16MHz,restart_wdt)

#define MCU_BOARD_ID 0x02

//! PULSANTE
#define BTN   PIN_B0

//! LED
#define ST3   PIN_D0
#define ST2   PIN_D1
#define ST1   PIN_D2

//! CANBUS CTRL PIN
#define CAN_SLEEP_MODE output_high(PIN_B4)
#define CAN_HIGH_SPEED output_low(PIN_B4)
#define CAN_LOW_SPEED input(PIN_B4)  //! RS=47K -> 8V/usec

//! UART2
//!#use rs232(baud=115200,parity=N,UART2,bits=8,stream=PORT2,ENABLE=DE_485)
#bit OERR2= 0xFA6.1
#bit CREN2= 0xFA6.4
//! if (OERR2) {CREN2=0; CREN2=1;}; //! inserire come controllo in caso si bloccasse la seriale

//! UART1
#if SET_CMD_ID == GPS
   #use rs232(baud=4800,parity=N,UART1,bits=8,stream=PORT1)
   //#use rs232(baud=115200,parity=N,UART1,bits=8,stream=PORT1/*,ENABLE=PIN_XX*/)
#endif
#bit OERR1= 0xFAB.1
#bit CREN1= 0xFAB.4
//! if (OERR1) {CREN1=0; CREN1=1;}; //! inserire come controllo in caso si bloccasse la seriale

//! CONTROLLO ACCELERATORE VIA SPI
#use spi(MASTER, SPI1, BAUD=1000000, MODE=0, BITS=8, STREAM=SPI_ACC)
//! CHIP SELECT
#define CS_ACC J3_12  //! DAC
#define LDAC J3_10

//! CHIP SELECT
//!#define CS J5_12  //! DAC  

//! CONNETTORE ESPANSIONE J3
#define J3_2 PIN_A0  //! AN0
#define J3_3 PIN_B1  //! INT1/AN8
#define J3_4 PIN_A1  //! AN1
#define J3_5 PIN_D4
#define J3_6 PIN_A2  //! AN2/Vref-
#define J3_7 PIN_C7  //! RX1
#define J3_8 PIN_A3  //! AN3/Vref+
#define J3_9 PIN_C6  //! TX1
#define J3_10 PIN_A5 //! AN4
#define J3_11 PIN_C5 //! SPI1_SDO
#define J3_12 PIN_E0 //! AN5
#define J3_13 PIN_C4 //! SPI1_SDI
#define J3_14 PIN_C3 //! SPI1_SCK


//! CONNETTORE ESPANSIONE J1
#define J1_2 PIN_D3  
#define J1_3 PIN_B5
#define J1_4 PIN_D5
#define J1_5 PIN_D6  //! TX2
#define J1_6 PIN_D7  //! RX2
#define J1_7 PIN_C1  
#define J1_8 PIN_C2  
#define J1_9 PIN_C0  


#define lsb(x) *((int8*)&x)
#define msb(x) *((int8*)&x+1)
#define mmsb(x) *((int8*)&x+2)
#define mmmsb(x) *((int8*)&x+3)
