#if SET_CMD_ID == Acceleratore_Odometria
   #include "set_cmd_acc_odo.c"
#elif SET_CMD_ID == Steerig_Brake
   #include "set_cmd_steering_brake.c"
#elif SET_CMD_ID == GPS 
   #include "set_cmd_gps.c"
#elif SET_CMD_ID == Acceleratore 
   #include "set_cmd_acc.c"
#endif

/* DEFINIZIONE ID CAN RX*/
// comandi generici comini alle diverse schede set_cmd_ID 0x07
#define GET_HW_ID          0x000007FE // risp. [0]hw_address|[1]CMD_SET_ID|[2]MCU_BOARD_ID
#define GET_GPS_VEL        0x00000305

void gestione_comando_generico(unsigned int32 frame_ID,unsigned int8 *data)
{
  switch(frame_ID)
  {
      case GET_HW_ID :
         CAN_OUT_DATA[0]=hw_address;
         CAN_OUT_DATA[1]=SET_CMD_ID;
         CAN_OUT_DATA[2]=MCU_BOARD_ID;
         CAN_OUT_DATA_LEN=3;
         CAN_OUT_ID=frame_ID+1;
         can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
      break;
      
      case GET_GPS_VEL :
    	 GPS_RECEIVED = 1;
         current_vel = (float32)(*( (unsigned int16*)( data+6 ) )) / 1000.0;
      break;
      
      default:
      break;
  }
}
