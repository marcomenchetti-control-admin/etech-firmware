#include "gps_01.c"

// zona morta 2400..2600

//! 1tick=1,39mV
//! 2500mV/1,39=1798,5->1800
//!int16 v_offset=2500; //! offset della velocit� impostabile dall'utente
int16 v_offset=2048;
//! **********************************************************
// VEL_A=192+11*DATA
// VEL_B=388+32*DATA

//! BIT DI CONFIGURAZIONE PER IL DAC
//! <A#/B><BUF><GA#><SHDN#><D11-D0>
//!unsigned int16 conf_A=6;
unsigned int16 conf_A=0b0011000000000000;
// VEL_A=192+11*DATA
//!unsigned int16 conf_B=14;
unsigned int16 conf_B=0b1011000000000000;
// VEL_B=388+32*DATA
//! END BIT DI CONFIGURAZIONE PER IL DAC

void set_vel(unsigned int8 dirA,unsigned int8 velA,unsigned int8 dirB,unsigned int8 velB)
{   
   signed int16 aux=0;
   unsigned int16 vel_a=0;
   unsigned int16 vel_b=0;
   //! per sicurezza in caso di dato errato metto al minimo
   disable_interrupts(GLOBAL);

   if(velA>100)
   {velA=100;}      

   if(velB>100)
   {velB=100;}    

   aux=velA;
   if(dirA==1) // reverse
      aux=-aux;

   vel_a=(v_offset)+20*aux;
   
   aux=velB;
   if(dirB==1) // reverse
      aux=-aux;

   vel_b=(v_offset)+20*aux;
   
   // controllo di non uscire dai limiti
   if(vel_b>=4095)
   {vel_b=4095;}
   if(vel_a>=4095)
   {vel_a=4095;}
   
   aux=conf_A|vel_a;
   output_low(CS_ACC);
   spi_xfer(SPI_ACC,aux,16);
   output_high(CS_ACC);
   delay_us(10);

   aux=conf_B|vel_b;
   output_low(CS_ACC);
   spi_xfer(SPI_ACC,aux,16);
   output_high(CS_ACC);
   delay_us(10);
   
   //! IMPULSO SEGNALE LDAC
   output_low(LDAC);
   delay_us(10);
   output_high(LDAC);
   enable_interrupts(GLOBAL);
}

/* DEFINIZIONE ID CAN RX*/
// comandi per la scheda che controlla gps  set_cmd_ID 0x03
#define SET_ACTIVE_ID      0x00000302 // payload 2 [0]:addr_hw [1]:abilita il flusso dati
#define INIT_GPS_ID        0x00000308 // payload 1 [0]:addr_hw 
/*
FLUSSO DATI CON FIX ACQUISITO
0x00000304 -> DATA,ORA
0x00000305 -> LAT,SPEED
0x00000306 -> LON,COMPASS

FLUSSO DATI CON FIX NON ACQUISITO
0x00000307 -> NO FIX
*/

unsigned int1 enable_gps=TRUE; // flag che indica se il flusso dati gps � abilitato

void gestione_comando(unsigned int32 frame_ID,unsigned int8 *data)
{
switch(frame_ID)
  {
      case SET_ACTIVE_ID :
         if(*(data+0)==hw_address)
         {
            CAN_OUT_DATA[0]=hw_address;
            if(bit_test(*(data+1),0)) //! disabilito il flusso dati
               {
                  CAN_OUT_DATA[1]=0x01;
                  enable_gps=TRUE;  
               }
            else
               {
                  CAN_OUT_DATA[1]=0x00;
                  enable_gps=FALSE;                
               }
            CAN_OUT_DATA_LEN=2;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
         }
      break;

      case INIT_GPS_ID :
         if(*(data+0)==hw_address)
         {
            CAN_OUT_DATA[0]=hw_address;
            CAN_OUT_DATA[1]=*(data+1);
            CAN_OUT_DATA_LEN=2;
            CAN_OUT_ID=frame_ID+1;
            can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
            init_GPS(CAN_OUT_DATA[1]);
         }
      break;     
     
      default:
      break;
  }
}


void send_GPS_via_CAN(void)
{
   if(fix=='A' && checksum_valid)
   {
      CAN_OUT_ID=0x00000304;
      CAN_OUT_DATA_LEN=8;
      CAN_OUT_DATA[0]=hw_address;
      CAN_OUT_DATA[1]=giorno; // DD
      CAN_OUT_DATA[2]=mese;   //MM
      CAN_OUT_DATA[3]=anno;   //YY
      CAN_OUT_DATA[4]=(int8)ora; //HH
      CAN_OUT_DATA[5]=(int8)minuti; //MM
      CAN_OUT_DATA[6]=(int8)secondi; //SS
      CAN_OUT_DATA[7]=(int8)centesimi; //1/100 SEC
      can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
      
      CAN_OUT_ID=0x00000305;
      CAN_OUT_DATA_LEN=8;
      CAN_OUT_DATA[0]=hw_address;
      CAN_OUT_DATA[1]=lat_dir; // Lat Dir
      CAN_OUT_DATA[2]=lat_deg; // deg
      CAN_OUT_DATA[3]=lat_min; // deg min
      CAN_OUT_DATA[4]=lsb(lat_sec); // deg sec h
      CAN_OUT_DATA[5]=msb(lat_sec); // deg sec l
      CAN_OUT_DATA[6]=lsb(speed); // speed h
      CAN_OUT_DATA[7]=msb(speed); // speed l
      can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
      
      CAN_OUT_ID=0x00000306;
      CAN_OUT_DATA_LEN=8;
      CAN_OUT_DATA[0]=hw_address;
      CAN_OUT_DATA[1]=lon_dir; // Lon Dir
      CAN_OUT_DATA[2]=lon_deg; // deg
      CAN_OUT_DATA[3]=lon_min; // deg min
      CAN_OUT_DATA[4]=lsb(lon_sec); // deg sec h
      CAN_OUT_DATA[5]=msb(lon_sec); // deg sec l
      CAN_OUT_DATA[6]=lsb(compass); // compass h
      CAN_OUT_DATA[7]=msb(compass); // compass l
      can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
      
//!      CAN_OUT_ID=0x00000307;
//!      CAN_OUT_DATA_LEN=6;
//!      CAN_OUT_DATA[0]=hw_address;
//!      CAN_OUT_DATA[1]=fix; // FIX
//!      CAN_OUT_DATA[2]=msb(speed); // speed h
//!      CAN_OUT_DATA[3]=lsb(speed); // speed l
//!      CAN_OUT_DATA[4]=msb(compass); // compass h
//!      CAN_OUT_DATA[5]=lsb(compass); // compass l
//!      can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);
   }
   else // non ho raggiunto il FIX della posizione
   {
      CAN_OUT_ID=0x00000307;
      CAN_OUT_DATA_LEN=2;
      CAN_OUT_DATA[0]=hw_address;
      CAN_OUT_DATA[1]='V'; //! fix; // FIX
      can_putd(CAN_OUT_ID,&CAN_OUT_DATA[0],CAN_OUT_DATA_LEN,PRIORITY,EXT,RTR);   
   }
}

