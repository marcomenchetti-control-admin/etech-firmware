# ICD: GPS Can Adapter

## Messages

### Generic

#### Get hardware info

This command returns the hardware info of the controller board

|         | NAME      | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)               |
| ------- | --------- | ----- | -------- | ------ | ------------------------------------ |
| Command | GET_HW_ID | 0x7FE | true     | 0      |                                      |
| Answer  |           | 0x7FF | true     | 3      | [hw_address,SET_CMD_ID,MCU_BOARD_ID] |

Where:

- hw_address represents the unique address of the board
- SET_CMD_ID represents the functionality of the board
  - for the GPS it will always be 0x03.
- MCU_BOARD_ID **DUNNO**

### Specific

#### Input messages

##### Activate GPS data-stream

This command activate the stream of data on the can bus

|         | NAME          | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte) |
| ------- | ------------- | ----- | -------- | ------ | ---------------------- |
| Command | SET_ACTIVE_ID | 0x302 | true     | 2      | [hw_address,enable]    |
| Answer  |               | 0x303 | true     | 2      | [hw_address,enable]    |

Where:

- hw_address:see above
- enable is a bool value (expressed as 0 or 1) which enables or disable the communication on the can bus

##### Initialize GPS

This command initialize the GPS with the current working frequency.

|         | NAME          | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)       |
| ------- | ------------- | ----- | -------- | ------ | ---------------------------- |
| Command | SET_ACTIVE_ID | 0x308 | true     | 2      | [hw_address,publishing_rate] |
| Answer  |               | 0x309 | true     | 2      | [hw_address,publishing_rate] |

Where:

- hw_address: see above
- publishing_rate represents the rate at which data are published on the can bus
  - 0x01 means 1Hz
  - 0x05 means 5Hz
  - 0x0A means 10Hz

#### Output messages

##### Date-Time

This message publishes the data relative to the date and time

|         | NAME | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                           |
| ------- | ---- | ----- | -------- | ------ | ------------------------------------------------ |
| Message |      | 0x304 | true     | 8      | [hw_address,day,month,year,hour,min,sec,cen_sec] |

Where:

- cen_sec are hundredths of seconds

##### Lat-Speed

This message publishes the data relative to the latitude and speed

|         | NAME | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                                                     |
| ------- | ---- | ----- | -------- | ------ | -------------------------------------------------------------------------- |
| Message |      | 0x305 | true     | 8      | [hw_address,lat_dir,lat_deg,lat_min,lat_sec_mil (2 bytes),speed (2 bytes)] |

Where:

- lat_dir represents the hemisphere (N or S)
- lat_deg represents the degree of latitude
- lat_min represents the minutes of latitude
- lat_sec represents the seconds of latitude multiplied by 1000
- speed represents the measured speed expressed in [mm]/[s]

##### Lon-Course

This message publishes the data relative to the longitude and course

|         | NAME | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                                                      |
| ------- | ---- | ----- | -------- | ------ | --------------------------------------------------------------------------- |
| Message |      | 0x306 | true     | 8      | [hw_address,lon_dir,lon_deg,lon_min,lon_sec_mil (2 bytes),course (2 bytes)] |

Where:

- lon_dir represents the hemisphere (N or S)
- lon_deg represents the degree of longitude
- lon_min represents the minutes of longitude
- lon_sec represents the seconds of longitude multiplied by 1000
- course represents the measured course of the path measured in degrees

##### No signal

This message informs the user that the GPS is not receiving a fix message

|         | NAME | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte) |
| ------- | ---- | ----- | -------- | ------ | ---------------------- |
| Message |      | 0x307 | true     | 2      | [hw_address,0x56]      |

Where:

- 0x56 represent the character 'V', used in NMEA strings to represent the lack of fix
