# ICD: Thruster Controller

## Messages

### Generic

#### Get hardware info

This command returns the hardware info of the controller board

|         | NAME      | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)               |
| ------- | --------- | ----- | -------- | ------ | ------------------------------------ |
| Command | GET_HW_ID | 0x7FE | true     | 0      |                                      |
| Answer  |           | 0x7FF | true     | 3      | [hw_address,SET_CMD_ID,MCU_BOARD_ID] |

Where:

- hw_address represent the unique address of the board
- SET_CMD_ID represent the functionality of the board
  - for the controller it will always be 0x06.
- MCU_BOARD_ID **DUNNO**

#### Input GPS command

This command is received from another CAN device and it contains the current velocity of the vehicle.
Although it is not the only data contained, it is the only one used.

|         | NAME        | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                         |
| ------- | ----------- | ----- | -------- | ------ | ---------------------------------------------- |
| Command | GET_GPS_VEL | 0x305 | true     | 8      | [unused_data (6 bytes), current_vel (2 bytes)] |
| Answer  |             |       |          |        |                                                |

Where:

- current_vel is an int16 representing the velocity of the vehicle expressed as [mm]/[s]

### Specific

#### Set thrusters command

This command set the command to the motors between [100,-100]

|         | NAME       | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                  |
| ------- | ---------- | ----- | -------- | ------ | --------------------------------------- |
| Command | SET_ACC_ID | 0x602 | true     | 5      | [hw_address,dir_a, vel_a, dir_b, vel_b] |
| Answer  |            | 0x603 | true     | 5      | [hw_address,dir_a, vel_a, dir_b, vel_b] |

Where:

- hw_address is a unique identifier of the board
- dir_a and dir_b represent the direction of motion of the two motors.
  - 1 means backward
  - 0 means forward
- vel_a and vel_b represent the module of the control input, between 0 and 100

#### Get thrusters command

This command retrieves the command control input that is being provided to the motors

|         | NAME       | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                  |
| ------- | ---------- | ----- | -------- | ------ | --------------------------------------- |
| Command | GET_ACC_ID | 0x604 | true     | 1      | [hw_address]                            |
| Answer  |            | 0x605 | true     | 5      | [hw_address,dir_a, vel_a, dir_b, vel_b] |

For payload see above.

#### Enable closed loop controller

This command chooses the type of controller based on the value of *enable*:

- 0 means the controller simply use the value provided by the higher level

- 1 means the controller actuates a velocity provided from the higher level
  
  |         | NAME        | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte) |
  | ------- | ----------- | ----- | -------- | ------ | ---------------------- |
  | Command | USE_GPS_VEL | 0x61A | true     | 2      | [hw_address,enable]    |
  | Answer  |             | 0x61B | true     | 2      | [hw_address,enable]    |

#### Enable conroller

This command enables or disable the controller board in favor of the throttle joysticks.
This means that the control input provided to the motors comes from a pilot.

|         | NAME          | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte) |
| ------- | ------------- | ----- | -------- | ------ | ---------------------- |
| Command | ENABLE_VEL_ID | 0x61C | true     | 2      | [hw_address,enable]    |
| Answer  |               | 0x61D | true     | 2      | [hw_address,enable]    |

#### Set PID gain

This command sets the gains of the closed loop controller on the speed of the vehicle.

|         | NAME     | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                  |
| ------- | -------- | ----- | -------- | ------ | --------------------------------------- |
| Command | SET_GAIN | 0x630 | true     | 6      | [hw_address,gain_id,gain (4 bytes)]     |
| Answer  |          | 0x631 | true     | 6      | [hw_address,gain_id,act_gain (4 bytes)] |

Where:

- hw_address: see above
- gain_id identifies the specific gain:
  - 0 means proportional
  - 1 means integral
  - 2 means derivative
- gain represent a [float32](https://en.wikipedia.org/wiki/IEEE_754#:~:text=The%20IEEE%20Standard%20for%20Floating-Point%20Arithmetic%20is%20a,of%20binary%20and%20decimal%20floating-point%20data%2C%20which%20) containing the new gain.
- act_gain represent the gain before the new one is set.

#### Get PID gain

This command gets the gains of the closed loop controller on the speed of the vehicle.

|         | NAME     | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)                  |
| ------- | -------- | ----- | -------- | ------ | --------------------------------------- |
| Command | SET_GAIN | 0x632 | true     | 2      | [hw_address,gain_id]                    |
| Answer  |          | 0x633 | true     | 6      | [hw_address,gain_id,act_gain (4 bytes)] |

Where:

- hw_address: see above
- gain_id: see above
- gain: see above
- act_gain: see above

#### Set target vel

This command is used to pass the velocity reference to the controller.

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)             |
| ------- | ------------ | ----- | -------- | ------ | ---------------------------------- |
| Command | SET_TRGT_VEL | 0x634 | true     | 5      | [hw_address,target_vel (4 bytes)]  |
| Answer  |              | 0x635 | true     | 5      | [hw_address,current_vel (4 bytes)] |

Where:

- target_vel is a float32 value representing the desired velocity

- current_vel is a float32 value representing the current velocity of the vehicle.
  **NOTE: this relies on the presence of a low level GPS**

#### Get target vel

This command is used to retrieve the last velocity reference provided to the vehicle.

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)            |
| ------- | ------------ | ----- | -------- | ------ | --------------------------------- |
| Command | GET_TRGT_VEL | 0x636 | true     | 1      | [hw_address]                      |
| Answer  |              | 0x637 | true     | 5      | [hw_address,target_vel (4 bytes)] |

#### Get current_vel

This command is used to retrieve the current velocity of the vehicle.
**NOTE: here, as above, we rely on the presence of a low level GPS**

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)             |
| ------- | ------------ | ----- | -------- | ------ | ---------------------------------- |
| Command | GET_CURR_VEL | 0x638 | true     | 1      | [hw_address]                       |
| Answer  |              | 0x639 | true     | 5      | [hw_address,current_vel (4 bytes)] |

#### Set target angular velocity

> **NOTE: TODO with IMU and returning the current angular velocity**

This command is used to pass the angular velocity reference to the controller.

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)             |
| ------- | ------------ | ----- | -------- | ------ | ---------------------------------- |
| Command | SET_TRGT_OME | 0x63A | true     | 5      | [hw_address,target_ome (4 bytes)]  |
| Answer  |              | 0x63B | true     | 5      | [hw_address, target_ome (4 bytes)] |

Where:

- target_ome is a float32 value representing the desired velocity

#### Get target angular velocity

This command is used to retrieve the last angular velocity reference provided to the vehicle.

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)            |
| ------- | ------------ | ----- | -------- | ------ | --------------------------------- |
| Command | GET_TRGT_OME | 0x63C | true     | 1      | [hw_address]                      |
| Answer  |              | 0x63D | true     | 5      | [hw_address,target_ome (4 bytes)] |

#### Get current angular velocity

> **NOTE: TODO ALL with IMU**

This command is used to retrieve the current  angular velocity of the vehicle.
**NOTE: here, as above, we rely on the presence of a low level IMU**

|         | NAME         | ID           | EXTENDED | LENGTH | PAYLOAD(byte per byte)             |
| ------- | ------------ | ------------ | -------- | ------ | ---------------------------------- |
| Command | GET_CURR_OME | NOT PROVIDED | true     | 1      | [hw_address]                       |
| Answer  |              | NOT PROVIDED | true     | 5      | [hw_address,current_ome (4 bytes)] |

#### Set proportional coefficient for angular velocity

> **NOTE: MAKE IT A PID WHEN IMU AVAILABLE**

|         | NAME         | ID    | EXTENDED | LENGTH | PAYLOAD(byte per byte)               |
| ------- | ------------ | ----- | -------- | ------ | ------------------------------------ |
| Command | OME_COEFF_ID | 0x63E | true     | 5      | [hw_address,ome_coeff (4 bytes)]     |
| Answer  |              | 0x63F | true     | 5      | [hw_address,act_ome_coeff (4 bytes)] |

Where:

- ome_coeff is the new float32 value representing the proportional coefficient to model the actuation of the angular velocity
- act_ome_coeff is the actual float32 value representing the above coefficient
