//! DEFINE DEI SET DI COMANDI
#define Acceleratore_Odometria  0x01
#define Steerig_Brake           0x02
#define GPS                     0x03
//!#define 0x04
#define CAN_to_ADC              0x05
#define Acceleratore            0x06
#define Generic                 0x07 // comandi comuni a tutti i dispositivi 
//! FINE DEFINE DEI SET DI COMANDI

// #define SET_CMD_ID Acceleratore_Odometria
 #define SET_CMD_ID Acceleratore
//! #define SET_CMD_ID GPS

//! DEFINE della velocit� del CANBUS
//! #define CAN_DEF_BAUD_500K
 #define CAN_DEF_BAUD_125K
//! #define CAN_DEF_BAUD_50K
//! FINE DEFINE della velocit� del CANBUS

//! #define debug

#if SET_CMD_ID == Acceleratore_Odometria
  #EXPORT(FILE=Acceleratore_Odometria_CAN_125K_V04.hex)
  #include "PEP_MCU.c" //! scheda MCU
#elif SET_CMD_ID == Steerig_Brake
  #EXPORT(FILE=Steerig_Brake_CAN_125K_V04.hex)
  #include "PEP_MCU.c" //! scheda MCU
#elif SET_CMD_ID == GPS
  #EXPORT(FILE=GPS_CAN_125K_V03.hex)
  #include "PEP30060101.c" //! scheda MCU
#elif SET_CMD_ID == Acceleratore
  #EXPORT(FILE=Acceleratore_CAN_125K_V01.hex)
  #include "PEP30060101.c" //! scheda MCU
#endif

unsigned int8 hw_address=0;

//! #include "PEP_MCU.c" //! scheda MCU
//! #include "PEP30060101.c" //! scheda MCU

#INCLUDE <stdlib.h>

//! #include "PRG62_func.c" //! funzioni del progetto 62

   
#ifdef CAN_DEF_BAUD_500K
   #define CAN_BRG_PRESCALAR           3    //Set CAN Baud Rate to 500K
   #define CAN_BRG_PHASE_SEGMENT_1     6    //Tq = (2*(1+PRESCALAR))/Fosc
   #define CAN_BRG_PHASE_SEGMENT_2     6    //Tq = (2*(1+3))/64000000 = 0,000000125
   #define CAN_BRG_PROPAGATION_TIME    0    //Baud Rate = 1/(((PHASE_SEGMENT_1+1)+(PHASE_SEGMENT_2+1)+(PROPAGATION_TIME+1)+1)*Tq)
   #define CAN_BRG_SYNCH_JUMP_WIDTH    0    //Baud Rate = 1/(((6+1)+(6+1)+(0+1)+1)*0,000000125) = 500000
#endif
#ifdef CAN_DEF_BAUD_125K
   #define CAN_BRG_PRESCALAR           15    //Set CAN Baud Rate to 125K
   #define CAN_BRG_PHASE_SEGMENT_1     6    //Tq = (2*(1+PRESCALAR))/Fosc
   #define CAN_BRG_PHASE_SEGMENT_2     6    //Tq = (2*(1+15))/64000000 = 0,0000005
   #define CAN_BRG_PROPAGATION_TIME    0    //Baud Rate = 1/(((PHASE_SEGMENT_1+1)+(PHASE_SEGMENT_2+1)+(PROPAGATION_TIME+1)+1)*Tq)
   #define CAN_BRG_SYNCH_JUMP_WIDTH    0    //Baud Rate = 1/(((6+1)+(6+1)+(0+1)+1)*0,0000005) = 125000
#endif
#ifdef CAN_DEF_BAUD_50K
   #define CAN_BRG_PRESCALAR           39    //Set CAN Baud Rate to 125K
   #define CAN_BRG_PHASE_SEGMENT_1     6    //Tq = (2*(1+PRESCALAR))/Fosc
   #define CAN_BRG_PHASE_SEGMENT_2     6    //Tq = (2*(1+39))/64000000 = 0,00000125
   #define CAN_BRG_PROPAGATION_TIME    0    //Baud Rate = 1/(((PHASE_SEGMENT_1+1)+(PHASE_SEGMENT_2+1)+(PROPAGATION_TIME+1)+1)*Tq)
   #define CAN_BRG_SYNCH_JUMP_WIDTH    0    //Baud Rate = 1/(((6+1)+(6+1)+(0+1)+1)*0,00000125) = 50000
#endif

#include <can-18F4580.c>

#define MAX_LEN 8

//! VARIABILI CAN
struct rx_stat rxstat;
unsigned int8 CAN_IN_DATA[MAX_LEN];
unsigned int32 CAN_IN_ID;
unsigned int8 CAN_IN_DATA_LEN;

unsigned int8 CAN_OUT_DATA[MAX_LEN];
unsigned int32 CAN_OUT_ID;
unsigned int8 CAN_OUT_DATA_LEN=8;



float32 T_s = 0.1;
float32 freq_s = 10;
typedef struct{
   float32 P;
   float32 I;
   float32 D;
} gains;
gains PID_controller_VEL = {2.0,0,0};

int1 USE_CONTROL = 0;
int1 GPS_RECEIVED = 0;
int1 GPS_ABSENT = 1;
unsigned int8 gps_absent_counter = 0;
float32 err = 0;
float32 errI = 0;
float32 errD = 0;
float32 target_vel = 0;
float32 current_vel = 0;
float32 target_omega = 0;
float32 omega_coefficient = 0; // 100/MAX_ANGULAR_VELOCITY

#include "gestione_messaggi_can.c"

#INT_TIMER0
void control_loop() {
   set_timer0(0xE796); //! 65536 - 6250(ticks at T=16us -> 100ms) = 59287 ticks = 0xE796
   /*check gps*/
	if(!GPS_RECEIVED) {
		if(gps_absent_counter < 50) {
			gps_absent_counter++;
		} else if(gps_absent_counter >= 50) {
			GPS_ABSENT = (int1)1;
		}
	} else {
		gps_absent_counter = 0;
		GPS_ABSENT = (int1)0;
	}
	GPS_RECEIVED = 0;
	/*//for tests
	if( USE_CONTROL && !GPS_ABSENT) {
		set_vel(0,100,0,100);
	} else if( USE_CONTROL && GPS_ABSENT) {
		set_vel(0,1,0,1);
	} else {
		set_vel(1,100,1,100);
	}*/
   	
	if(USE_CONTROL && !GPS_ABSENT) {
      float32 e = target_vel - current_vel;
      errD = (e - err) * PID_controller_VEL.D;
      errI += e * PID_controller_VEL.I;
      if(errI>=50.0) errI = 50.0;
      else if(errI <= -50.0) errI = -50.0;
      err = e;
      
      float32 u = err * PID_controller_VEL.P + errD + errI; //mean value
      float32 delta = omega_coefficient * target_omega;
      
      int8 dir_L = (u-delta) > 0 ? 1 : 0; //(+u-delta) ?
      int8 dir_R = (u+delta) > 0 ? 0 : 1; //(-u-delta) ?
      int8 sign_L = (u-delta) > 0 ? 1 : -1;
      int8 sign_R = (u+delta) > 0 ? 1 : -1;
      
      set_vel( dir_R, sign_R * (int8)( u+delta ), dir_L, sign_L * (int8)( u-delta ) );
      
   } else if(USE_CONTROL && GPS_ABSENT) {
	  float32 u = target_vel * 25.0; // target vel * 100 / max_vel; max_vel = 4;
      float32 delta = omega_coefficient * target_omega;
      
      int8 dir_L = (u-delta) > 0 ? 1 : 0; //(+u-delta) ?
      int8 dir_R = (u+delta) > 0 ? 0 : 1; //(-u-delta) ?
      int8 sign_L = (u-delta) > 0 ? 1 : -1;
      int8 sign_R = (u+delta) > 0 ? 1 : -1;
      
      set_vel( dir_R, sign_R * (int8)( u+delta ), dir_L, sign_L * (int8)( u-delta ) );
      errI = 0;
   } else {
	  errI = 0;
   }
}

void main()
{   
   setup_adc_ports(sAN6|sAN7); // canali letture DIPSWITCH
   setup_adc(ADC_CLOCK_INTERNAL|ADC_TAD_MUL_0);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0xc0,2);
   
   //disable_interrupts(GLOBAL);
   
   #if SET_CMD_ID == GPS
      enable_interrupts(INT_RDA);
      enable_interrupts(GLOBAL);
   #elif SET_CMD_ID == Acceleratore_Odometria
      enable_vel(FALSE);
      set_vel(0);
      //! calcolo parametri della odometria
      update_param();
   #elif SET_CMD_ID == Steerig_Brake
      enable_interrupts(INT_RDA);
      enable_interrupts(GLOBAL); 
      set_com_port(RS232);
      
      output_low(on_off);
      output_low(dir_brake);
      output_low(pulse_brake);
   #elif SET_CMD_ID == Acceleratore
      enable_vel(FALSE);
   #endif
     
   can_init();
     
   can_set_mode(CAN_OP_CONFIG); 
   can_set_functional_mode(CAN_FUN_OP_LEGACY);
  
   can_set_mode(CAN_OP_NORMAL);

   delay_ms(500);   
   //! test LED
   output_high(ST1);
   output_high(ST2);
   output_high(ST3);
   delay_ms(500);
   output_low(ST1);
   output_low(ST2);
   output_low(ST3);
   //! end test LED
   delay_ms(500);   
   #if  SET_CMD_ID == GPS
      init_GPS(1);
   #endif  
   //! test LED
   output_high(ST1);
   output_high(ST2);
   output_high(ST3);
   delay_ms(500);
   output_low(ST1);
   output_low(ST2);
   output_low(ST3);
   //! end test LED
   #if  SET_CMD_ID == GPS
      init_GPS(1);
   #endif
   delay_ms(500);
   
   #if SET_CMD_ID == Acceleratore_Odometria
//!      setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);  //  clock 64 mhz   , div 4  div 8   clock 2 mhz
//!      set_timer1(45536); //! FOSC4=16M - T1_DIV_8=2M - tick(20K)=10ms - postscaler(10)=100ms
//!      enable_interrupts(INT_EXT_L2H);
//!      enable_interrupts(INT_EXT1_L2H);
//!      enable_interrupts(INT_TIMER1);
//!      enable_interrupts(GLOBAL);
               setup_timer_1(T1_DISABLED);
               disable_interrupts(INT_EXT);
               disable_interrupts(INT_EXT1);
               disable_interrupts(INT_TIMER1);
               disable_interrupts(GLOBAL); 
   #endif
   #if SET_CMD_ID == Acceleratore
      set_timer0(0xE796); //! 65536 - 6250(ticks at T=16us -> 100ms) = 59286 ticks = 0xE796
      setup_timer_0(T0_INTERNAL|T0_DIV_256);  //  clock 64 mhz   , div 4  div 8   clock 2 mhz
      enable_interrupts(INT_TIMER0);
      enable_interrupts(GLOBAL);
   #endif
   hw_address=get_hw_address(); // lettura del dipswitch
   //! hw_address=0x06;  // gas_odometria 
   while(TRUE)
   {
      if(OERR1) 
      {CREN1=0; CREN1=1;};
      
      if(OERR2)
      {CREN2=0; CREN2=1;};
      
      if(ms>50000)
      {
         ms=0;
      
         output_toggle(ST1);

      }
      
      #if SET_CMD_ID == Acceleratore_Odometria
         if(to_send)
         {
            //! output_toggle(ST2);
            send_odo();
            to_send=FALSE;
            //! output_toggle(ST2);
         }
         
         
         
         
      #endif

      #if SET_CMD_ID == GPS
         if(ricevutoGPS)
         {
//!         1. divido in pacchetti la stringa RMC
            separazione_campi();
//!         2. spedisco i pacchetti via CAN
            pacchetto_RMC=elaborazione_pacchetti();
            if(pacchetto_RMC && enable_gps)
            {
               send_GPS_via_CAN();
            }
            pacchetto_RMC=FALSE;
            ricevutoGPS=FALSE;
            purge_buffer_in();
            purge_pacchetti();
         }
      #endif     
     
      if(can_kbhit())
      {
        //! ms=0;
        //! output_toggle(ST1);
        //! can_get_id(CAN_RX_ID, int1 ext);
        can_getd(CAN_IN_ID, &CAN_IN_DATA[0], CAN_IN_DATA_LEN, rxstat);
   
        can_incoming_message(CAN_IN_ID, &CAN_IN_DATA[0]);
      }
      delay_us(10);
      ms++;
   }
}

