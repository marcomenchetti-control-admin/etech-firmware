//! parametri della funzione
//! can_putd(unsigned int32 id, unsigned int8 *data, unsigned int8 len, unsigned int8 priority, int1 ext, int1 rtr)
#define PRIORITY 0 // numero tra 0-3 valore alto max priorit�
#define EXT      1 // 1 extended id
#define RTR      0 // se settato a 1 richiede la risposta automatica

#include "set_cmd_generic.c"

#define CAN_PORT      0
#define UART1_PORT    1
#define UART2_PORT    2
//!   PORT = 0:CAN | 1:UART1 | 2:UART2
//!   ENABLE = 0:DISABLE | 1:ENABLE | 2:ENABLE_RS232 | 3:ENABLE_RS422 | 4:ENABLE_RS485 

#define CAN_BAUD_500K      3
#define CAN_BAUD_250K      2
#define CAN_BAUD_125K      1 // default
#define CAN_BAUD_50K       0

#define UART_BAUD_115200   3
#define UART_BAUD_57600    2
#define UART_BAUD_19200    1 // default
#define UART_BAUD_9600     0

//! RISPOSTE AD UNA DOMANDA
//! ID_RISPOSTA = ID_DOMANDA+1
//! DATA[0]=hw_address



void purge_buffer(unsigned int8 *data)
{
   int8 i=0;
   for(i=0;i<MAX_LEN;i++)
   {
      *(data+i)=0;
   }
}


void can_incoming_message(unsigned int32 frame_ID,unsigned int8 *data)
{
//!   unsigned int8 i=0;
   unsigned int16 aux16=0;
   
   aux16=(frame_ID>>16);
   
   if(aux16!=0)
   {return;}
   
   aux16=(int16)(frame_ID);
   if(msb(aux16)!=SET_CMD_ID && !(msb(aux16)==Generic || msb(aux16) == GPS)) // non � un messaggio da trattare
   {return;}
   if(msb(aux16)==Generic || (msb(aux16) == GPS && SET_CMD_ID != GPS))
   {
      gestione_comando_generico(CAN_IN_ID, &CAN_IN_DATA[0]);
      return;
   }
   else
   {
      gestione_comando(CAN_IN_ID, &CAN_IN_DATA[0]);
   }
   purge_buffer(&CAN_OUT_DATA[0]);
   purge_buffer(&(data));
}


// *************************************************************************

#define T_LED_ON output_high(PIN_A0)
#define T_LED_OFF output_low(PIN_A0)
#define TASTO PIN_A1
#define PULLUP_TASTO output_high(PIN_A2)

int16 ms=0;
int16 ms_tasto=0;
   
#define MS_START 100 // tempo che di pressione del tasto per START/PAUSA della sequenza
#define MS_STOP  2000 // tempo che di pressione del tasto per STOP della sequenza

unsigned int8 AUX_IN_DATA[8];
unsigned int32 AUX_IN_ID;
unsigned int8 AUX_IN_DATA_LEN;

#inline
void gestione_tasto(void)
{
   if(!input(TASTO))
      {
         //!T_LED_ON;
         ms_tasto++;
         if(ms_tasto>=MS_STOP)
         {
            AUX_IN_ID=0x00000054;
            AUX_IN_DATA[0]=hw_address; // indirizzo scheda
            AUX_IN_DATA[1]=0x00; // STOP
            AUX_IN_DATA_LEN=2;
            can_incoming_message(AUX_IN_ID,&AUX_IN_DATA[0]);
            ms_tasto=MS_STOP;
         }
      }
      else
      {
         //!T_LED_OFF;
         if(ms_tasto>=MS_START && ms_tasto<MS_STOP)
         {
            AUX_IN_ID=0x00000054;
            AUX_IN_DATA[0]=hw_address; // indirizzo scheda
            AUX_IN_DATA[1]=0x02; // START
            AUX_IN_DATA[2]=0x01; // CICLI
            AUX_IN_DATA_LEN=3;
            can_incoming_message(AUX_IN_ID,&AUX_IN_DATA[0]);
         }
         ms_tasto=0;
      }
}
